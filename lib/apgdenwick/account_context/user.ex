defmodule APGDenwick.AccountContext.User do
  @moduledoc false

  use APGDenwick.Schema

  alias APGDenwick.AccountContext.User
  alias Comeonin.Bcrypt
  alias Ecto.Changeset

  import Ecto.Changeset

  schema "users" do
    field(:email)
    field(:password, :string, virtual: true)
    field(:password_hash)

    timestamps()
  end

  def changeset(%User{} = user, params) do
    user
    |> cast(params, [:email, :password])
    |> validate_required([:email, :password])
    |> validate_format(:email, ~r/@/)
    |> validate_password(:password)
    |> unique_constraint(:email)
    |> put_pass_hash()
  end

  def validate_password(changeset, field, options \\ []) do
    validate_change(changeset, field, fn _, password ->
      case valid_password?(password) do
        {:ok, _} -> []
        {:error, msg} -> [{field, options[:message] || msg}]
      end
    end)
  end

  defp valid_password?(password) when byte_size(password) > 7 do
    {:ok, password}
  end

  defp valid_password?(_), do: {:error, "The password is too short"}

  defp put_pass_hash(%Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Bcrypt.add_hash(password))
  end

  defp put_pass_hash(changeset), do: changeset
end
