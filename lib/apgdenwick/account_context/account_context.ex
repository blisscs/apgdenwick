defmodule APGDenwick.AccountContext do
  @moduledoc """
  Account Context
  """

  alias APGDenwick.AccountContext.User
  alias APGDenwick.Repo
  alias APGDenwickWeb.Endpoint
  alias Comeonin.Bcrypt
  alias Phoenix.Token

  @salt "Some random salt"
  @sign_in_fail_message "Email or Password is incorrect"

  def list_users do
    User
    |> Repo.all()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    user
    |> Repo.delete()
  end

  def create_user(attrs) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def get_user(id) do
    User
    |> Repo.get(id)
  end

  def get_user!(id) do
    User
    |> Repo.get!(id)
  end

  def get_user_by(map) do
    User
    |> Repo.get_by(map)
  end

  def get_user_by!(map) do
    User
    |> Repo.get_by!(map)
  end

  def sign_in(%{"email" => email, "password" => password}) do
    case get_user_by(%{email: email}) do
      nil ->
        {:error, @sign_in_fail_message}

      user ->
        case Bcrypt.check_pass(user, password) do
          {:ok, user} -> {:ok, Token.sign(Endpoint, @salt, user.id)}
          _ -> {:error, @sign_in_fail_message}
        end
    end
  end

  def verify_user_token(token) do
    {:ok, id} = Token.verify(Endpoint, @salt, token, max_age: 1_209_600)

    # Need to implement functionality for invalid token and expired token
    get_user!(id)
  end
end
