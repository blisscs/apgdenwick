defmodule APGDenwick.Schema do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      use Ecto.Schema

      @primary_key {:id, :binary_id, autogenerate: true}
      @derive {Phoenix.Param, key: :id}
    end
  end
end
