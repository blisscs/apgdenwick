defmodule APGDenwickWeb.Router do
  use APGDenwickWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", APGDenwickWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)

    get("/sign-in", SignInController, :index)
    get("/sign-out", SignInController, :sign_out)

    get("/dashboard", DashboardController, :index)
    get("/dashboard/users", DashboardUserController, :index)
  end

  # Other scopes may use custom stacks.
  scope "/api", APGDenwickWeb do
    pipe_through(:api)

    post("/sign-in", SignInController, :sign_in, as: :api_sign_in)
    resources("/users", UserController, only: [:show, :index])
  end
end
