defmodule APGDenwickWeb.UserView do
  use APGDenwickWeb, :view

  alias APGDenwickWeb.UserView

  def render("index.json", %{users: users}) do
    render_many(users, UserView, "user.json")
  end

  def render("show.json", %{user: user}) do
    render("user.json", %{user: user})
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, email: user.email}
  end
end
