defmodule APGDenwickWeb.SignInView do
  use APGDenwickWeb, :view

  def render("success.json", %{token: token}) do
    %{token: token}
  end

  def render("fail.json", %{error: error}) do
    %{error: error}
  end
end
