defmodule APGDenwickWeb.ChangesetView do
  use APGDenwickWeb, :view

  alias Ecto.Changeset

  def translate_errors(changeset) do
    Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json", %{changeset: changeset}) do
    %{errors: translate_errors(changeset)}
  end
end
