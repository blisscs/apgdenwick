defmodule APGDenwickWeb.SignInController do
  use APGDenwickWeb, :controller

  alias APGDenwick.AccountContext

  plug(:put_layout, "sign_in.html")

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def sign_in(conn, %{"sign_in" => sign_in_params}) do
    case AccountContext.sign_in(sign_in_params) do
      {:ok, token} ->
        render(conn, "success.json", token: token)

      {:error, error} ->
        render(conn, "fail.json", error: error)
    end
  end

  def sign_out(conn, _params) do
    render(conn, "sign_out.html")
  end
end
