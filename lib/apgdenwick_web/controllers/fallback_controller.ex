defmodule APGDenwickWeb.FallbackController do
  use APGDenwickWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(APGDenwickWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(APGDenwickWeb.ErrorView)
    |> render(:"404")
  end
end
