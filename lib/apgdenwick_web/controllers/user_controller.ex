defmodule APGDenwickWeb.UserController do
  use APGDenwickWeb, :controller

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User

  def index(conn, _params) do
    users = AccountContext.list_users()

    render(conn, "index.json", users: users)
  end

  def show(conn, %{"id" => id}) do
    user = AccountContext.get_user!(id)

    render(conn, "show.json", user: user)
  end
end
