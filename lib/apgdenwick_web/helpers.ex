defmodule APGDenwickWeb.Helpers do
  @moduledoc false

  import APGDenwickWeb.Router.Helpers

  alias APGDenwickWeb.Endpoint

  def apg_socket_url do
    url = page_url(Endpoint, :index)

    "#{Regex.replace(~r/^(http)/, url, "ws")}socket/websocket"
  end
end
