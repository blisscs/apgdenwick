defmodule APGDenwickWeb.UserChannel do
  @moduledoc false

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User
  alias APGDenwickWeb.ChangesetView
  alias APGDenwickWeb.UserView
  alias Ecto.Changeset

  use Phoenix.Channel

  def join("room:users", _message, socket) do
    {:ok, socket}
  end

  def handle_in("list_users", %{}, socket) do
    users = AccountContext.list_users()

    {:reply, {:ok, %{users: UserView.render("index.json", %{users: users})}}, socket}
  end

  def handle_in("delete_user", %{"id" => id}, socket) do
    if socket.assigns.current_user.id == id do
      {:reply, {:error, %{error: "Can't delete the logged in user."}}, socket}
    else
      {:ok, user} =
        id
        |> AccountContext.get_user!()
        |> AccountContext.delete_user()

      broadcast!(socket, "deleted_user", %{user: UserView.render("show.json", %{user: user})})

      {:reply, {:ok, %{user: UserView.render("show.json", %{user: user})}}, socket}
    end
  end

  def handle_in("create_user", %{"user" => user_params}, socket) do
    case AccountContext.create_user(user_params) do
      {:ok, %User{} = user} ->
        broadcast!(socket, "created_user", %{user: UserView.render("show.json", %{user: user})})
        {:reply, {:ok, %{user: UserView.render("show.json", %{user: user})}}, socket}

      {:error, %Changeset{} = changeset} ->
        {:reply, {:error, ChangesetView.render("error.json", %{changeset: changeset})}, socket}
    end
  end
end
