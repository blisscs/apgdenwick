# A.P.G. Denwick
## Company Website

[![pipeline status](https://gitlab.com/blisscs/apgdenwick/badges/master/pipeline.svg)](https://gitlab.com/blisscs/apgdenwick/commits/master)
[![coverage report](https://gitlab.com/blisscs/apgdenwick/badges/master/coverage.svg)](https://gitlab.com/blisscs/apgdenwick/commits/master)


Production | [Staging] (https://apgdenwick-staging.herokuapp.com/)

Status: Not Ready

## Tools used for development

Phoenix, Postgres, Elm

## Commands for development

1. **To boot up dev webserver**

````elixir
$ docker-compose run agpdenwick mix ecto.setup
$ docker-compose up
````

## Copyright and License

Copyright (c) 2018, Suracheth Chawla.

This A.P.G. Denwick Website Code is licensed under MIT License.