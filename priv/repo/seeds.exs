alias APGDenwick.AccountContext
alias APGDenwick.AccountContext.User

if Mix.env() == :dev do
  {:ok, %User{} = _user} =
    AccountContext.create_user(%{email: "admin@example.com", password: "password"})
end
