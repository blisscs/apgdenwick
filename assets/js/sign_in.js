import css from "../css/app.scss"
import sign_in from "../css/sign_in.scss"

const Elm = require("../Elm/SignIn.elm");
const mountNode = document.getElementById("elm-wrapper");

const ElmApp = Elm.SignIn.embed(mountNode, {sign_in_path: mountNode.dataset.sign_in_path, dashboard_path: mountNode.dataset.dashboard_path })

ElmApp.ports.saveTokenInLocalStorage.subscribe(function(token){
  localStorage.setItem('token', token);
});
