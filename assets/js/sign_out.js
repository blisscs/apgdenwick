localStorage.removeItem('token');

const mountNode = document.getElementById("elm-wrapper");

window.location = mountNode.dataset.after_sign_out_path;
