import css from "../css/app.scss"
import sign_in from "../css/dashboard.scss"

const mountNode = document.getElementById("elm-wrapper");
const token = localStorage.getItem('token');

if (token === null) {
  window.location = mountNode.dataset.sign_out_path;
} else {
  const Elm = require("../Elm/Dashboard.elm");
  const ElmApp = Elm.Dashboard.embed(mountNode, {sign_out_path: mountNode.dataset.sign_out_path, token: token, web_socket_url: mountNode.dataset.websocket_url });
}
