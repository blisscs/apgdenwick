module Dashboard exposing (main)

import Dashboard.Route exposing (..)
import Dashboard.Msg exposing (..)
import Dashboard.Sub exposing (..)
import Dashboard.Model exposing (..)
import Dashboard.Update exposing (..)
import Dashboard.View exposing (..)
import UrlParser as Url
import Navigation


type alias Flags =
    { sign_out_path : String
    , web_socket_url : String
    , token : String
    }


main : Program Flags Model Msg
main =
    Navigation.programWithFlags URLCHANGE
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags location =
    let
        route =
            Url.parsePath routeParser location

        paths =
            { sign_out_path = flags.sign_out_path
            , web_socket_url = flags.web_socket_url
            }

        token =
            flags.token

        model =
            { initialModel
                | paths = paths
                , token = token
                , route = route
            }
    in
        ( model, Cmd.none )
