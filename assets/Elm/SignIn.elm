port module SignIn exposing (main)

import Html exposing (..)
import SignIn.Msg exposing (Msg(..))
import SignIn.Model exposing (..)
import SignIn.Sub exposing (..)
import SignIn.Update exposing (..)
import SignIn.View exposing (..)


type alias Flags =
    { sign_in_path : String
    , dashboard_path : String
    }


main : Program Flags Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { initialModel
        | sign_in_path = flags.sign_in_path
        , dashboard_path = flags.dashboard_path
      }
    , Cmd.none
    )
