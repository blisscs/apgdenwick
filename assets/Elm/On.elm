module On exposing (..)

import Json.Decode as JD
import Html exposing (..)
import Html.Events exposing (..)


onKeyUp : (Int -> msg) -> Attribute msg
onKeyUp tagger =
    on "keyup" (JD.map tagger keyCode)
