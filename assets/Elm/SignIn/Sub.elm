module SignIn.Sub exposing (..)

import SignIn.Msg exposing (Msg(..))
import SignIn.Model exposing (..)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
