module SignIn.Msg exposing (..)

import Http
import SignIn.Decoder exposing (SuccessSignInResponse)


type Msg
    = NOACTION
    | PASSWORDCHANGE String
    | EMAILCHANGE String
    | SIGNIN
    | SIGNINSUBMITTED (Result Http.Error SuccessSignInResponse)
    | SHOWSIGNINAGAIN
    | ENTERPRESSSIGNIN Int
