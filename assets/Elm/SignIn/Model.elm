module SignIn.Model exposing (..)


type alias Model =
    { sign_in_path : String
    , dashboard_path : String
    , email : String
    , password : String
    , sign_in_result : SignInResult
    }


type SignInResult
    = NORESULT
    | SUCCESS
    | FAIL String


initialModel : Model
initialModel =
    { sign_in_path = ""
    , dashboard_path = ""
    , email = ""
    , password = ""
    , sign_in_result = NORESULT
    }
