port module SignIn.Update exposing (..)

import SignIn.Msg exposing (..)
import SignIn.Model exposing (..)
import Http exposing (..)
import Json.Encode as JE
import Json.Decode as JD
import SignIn.Decoder as SignInDecoder


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SHOWSIGNINAGAIN ->
            ( { model
                | sign_in_result = NORESULT
                , password = ""
              }
            , Cmd.none
            )

        SIGNINSUBMITTED (Ok result) ->
            ( { model
                | sign_in_result = SUCCESS
              }
            , saveTokenInLocalStorage result.token
            )

        SIGNINSUBMITTED (Err error) ->
            case error of
                Http.BadPayload _ response ->
                    let
                        body =
                            response.body

                        error =
                            case (JD.decodeString SignInDecoder.failSignInDecoder body) of
                                Ok errorRecord ->
                                    errorRecord.error

                                Err _ ->
                                    Debug.crash "Fail to parse fail signin result"
                    in
                        ( { model | sign_in_result = FAIL error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        NOACTION ->
            ( model, Cmd.none )

        PASSWORDCHANGE password ->
            ( { model | password = password }, Cmd.none )

        EMAILCHANGE email ->
            ( { model | email = email }, Cmd.none )

        ENTERPRESSSIGNIN 13 ->
            ( model, sign_in_command model )

        ENTERPRESSSIGNIN _ ->
            ( model, Cmd.none )

        SIGNIN ->
            ( model, sign_in_command model )


sign_in_command : Model -> Cmd Msg
sign_in_command model =
    let
        jsonBody =
            JE.object
                [ ( "sign_in"
                  , JE.object
                        [ ( "email", JE.string model.email )
                        , ( "password", JE.string model.password )
                        ]
                  )
                ]

        request =
            Http.request
                { method = "POST"
                , headers = []
                , url = model.sign_in_path
                , body = Http.jsonBody jsonBody
                , expect = Http.expectJson SignInDecoder.successSignInDecoder
                , timeout = Nothing
                , withCredentials = False
                }
    in
        Http.send SIGNINSUBMITTED request


port saveTokenInLocalStorage : String -> Cmd msg
