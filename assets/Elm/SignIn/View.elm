module SignIn.View exposing (..)

import SignIn.Model exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import SignIn.Msg exposing (..)
import On exposing (..)


view : Model -> Html Msg
view model =
    div [ class "text-center wrapper" ]
        [ case model.sign_in_result of
            NORESULT ->
                Html.form [ class "form-signin" ]
                    [ h1 [ class "h3 mb-3 font-weight-normal" ] [ text "Please Sign In" ]
                    , label [ for "inputEmail", class "sr-only" ] [ text "Email Address" ]
                    , input
                        [ type_ "email"
                        , id "inputEmail"
                        , class "form-control"
                        , placeholder "Email Address"
                        , autofocus True
                        , required True
                        , value model.email
                        , onInput EMAILCHANGE
                        , onKeyUp ENTERPRESSSIGNIN
                        ]
                        []
                    , label [ for "inputPassword", class "sr-only" ] [ text "Password" ]
                    , input
                        [ type_ "password"
                        , id "inputPassword"
                        , class "form-control"
                        , placeholder "Password"
                        , value model.password
                        , onInput PASSWORDCHANGE
                        , onKeyUp ENTERPRESSSIGNIN
                        ]
                        []
                    , button
                        [ class "btn btn-lg btn-primary btn-block"
                        , type_ "button"
                        , onClick SIGNIN
                        ]
                        [ text "Sign In" ]
                    ]

            SUCCESS ->
                p []
                    [ text "You are now Signed In!."
                    , br [] []
                    , a [ href model.dashboard_path ] [ text "Click Here To Login." ]
                    ]

            FAIL error ->
                p []
                    [ text error, br [] [], a [ href "#", onClick SHOWSIGNINAGAIN ] [ text "Click Here to Sign In Again" ] ]
        ]
