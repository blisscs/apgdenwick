module SignIn.Decoder exposing (..)

import Json.Decode exposing (string, Decoder)
import Json.Decode.Pipeline exposing (decode)


type alias FailSignInResponse =
    { error : String }


failSignInDecoder : Decoder FailSignInResponse
failSignInDecoder =
    decode FailSignInResponse
        |> Json.Decode.Pipeline.required "error" string


type alias SuccessSignInResponse =
    { token : String
    }


successSignInDecoder : Decoder SuccessSignInResponse
successSignInDecoder =
    decode SuccessSignInResponse
        |> Json.Decode.Pipeline.required "token" string
