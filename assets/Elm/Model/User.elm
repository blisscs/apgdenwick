module Model.User exposing (..)

import Json.Decode exposing (string, Decoder, list)
import Json.Decode.Pipeline exposing (decode)
import Json.Encode as JE


type alias User =
    { id : String
    , email : String
    }


type alias Form =
    { email : String
    , password : String
    }


type alias UserResponse =
    { user : User
    }


type alias UsersResponse =
    { users : List User }


type alias SingleTonError =
    { error : String }


type alias UserForm =
    { email : String, password : String }


type alias UserFormError =
    { email : List String, password : List String }


type alias FormErrorReponse =
    { errors : UserFormError }


userFormErrorDecoder : Decoder UserFormError
userFormErrorDecoder =
    decode UserFormError
        |> Json.Decode.Pipeline.optional "email" (list string) []
        |> Json.Decode.Pipeline.optional "password" (list string) []


formErrorResponseDecoder : Decoder FormErrorReponse
formErrorResponseDecoder =
    decode FormErrorReponse
        |> Json.Decode.Pipeline.required "errors" userFormErrorDecoder


singleTonErrorDecoder : Decoder SingleTonError
singleTonErrorDecoder =
    decode SingleTonError
        |> Json.Decode.Pipeline.required "error" string


userDecoder : Decoder User
userDecoder =
    decode User
        |> Json.Decode.Pipeline.required "id" string
        |> Json.Decode.Pipeline.required "email" string


userResponseDecoder : Decoder UserResponse
userResponseDecoder =
    decode UserResponse
        |> Json.Decode.Pipeline.required "user" userDecoder


usersResponseDecoder : Decoder UsersResponse
usersResponseDecoder =
    decode UsersResponse
        |> Json.Decode.Pipeline.required "users" (list userDecoder)


encodeUserForm : UserForm -> JE.Value
encodeUserForm user_form =
    JE.object
        [ ( "user"
          , JE.object
                [ ( "email", JE.string user_form.email )
                , ( "password", JE.string user_form.password )
                ]
          )
        ]
