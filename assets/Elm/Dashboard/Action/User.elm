module Dashboard.Action.User exposing (..)

import Model.User exposing (..)


type Action
    = SHOWDELETEDIALOG User
    | SHOWCREATEUSERDIALOG
    | CLOSEDIALOG
