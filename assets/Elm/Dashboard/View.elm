module Dashboard.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Dashboard.Model exposing (..)
import Dashboard.Msg exposing (..)
import Dashboard.Route exposing (..)
import Dashboard.Helpers exposing (..)
import Dashboard.View.User as UserView
import Dashboard.View.Dashboard as DashboardView


view : Model -> Html Msg
view model =
    div []
        [ nav [ class "navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0" ]
            [ a ([ class "navbar-brand col-sm-3 col-md-2 mr-0" ] ++ onClickPage DashboardRoute) [ i [ class "fas fa-home" ] [], text " Dashboard" ]
            , ul [ class "navbar-nav px-3" ]
                [ li [ class "nav-item text-nowrap" ]
                    [ a [ class "nav-link", href model.paths.sign_out_path ] [ text "Sign Out" ] ]
                ]
            ]
        , div [ class "container-fluid" ]
            [ div [ class "row" ]
                [ nav [ class "col-md-2 d-none d-md-block bg-light sidebar" ]
                    [ div [ class "sidebar-sticky" ]
                        [ ul [ class "nav flex-column" ]
                            [ li [ class "nav-item" ]
                                [ a
                                    ([ class "nav-link" ] ++ onClickPage DashboardUserRoute)
                                    [ text " Users "
                                    , i [ class "fas fa-users" ] []
                                    ]
                                ]
                            , li [ class "nav-item" ]
                                [ a [ class "nav-link", href "#" ]
                                    [ text " Projects "
                                    , i [ class "fas fa-book" ] []
                                    ]
                                ]
                            ]
                        ]
                    ]
                , node "main"
                    [ class "col-md-9 ml-sm-auto col-lg-10 pt-3 px-4", attribute "role" "main" ]
                    [ div [ class "d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom" ]
                        [ h1 [ class "h2" ] <|
                            case model.route of
                                Just DashboardRoute ->
                                    [ text "Dashboard" ]

                                Just DashboardUserRoute ->
                                    [ text "Users ", i [ class "fas fa-users" ] [] ]

                                Nothing ->
                                    Debug.crash "Not Possible"
                        ]
                    , case model.route of
                        Just DashboardRoute ->
                            DashboardView.view model

                        Just DashboardUserRoute ->
                            UserView.view model.user_model

                        Nothing ->
                            text ""
                    ]
                ]
            ]
        ]
