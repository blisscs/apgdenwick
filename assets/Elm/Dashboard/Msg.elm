module Dashboard.Msg exposing (..)

import Navigation
import Dashboard.Route exposing (..)
import Dashboard.Msg.User as UserMsg


type Msg
    = URLCHANGE Navigation.Location
    | GOTOROUTE Route
    | SUB String
    | USERGROUPMSG UserMsg.Msg
