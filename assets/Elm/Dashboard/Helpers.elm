module Dashboard.Helpers exposing (..)

import Dashboard.Msg exposing (..)
import Dashboard.Route exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (..)
import Html.Attributes exposing (..)
import Html exposing (..)


onClickPage : Route -> List (Attribute Msg)
onClickPage route =
    [ style [ ( "pointer", "cursor" ) ]
    , href (routeToString route)
    , onPreventDefaultClick (GOTOROUTE route)
    ]


onPreventDefaultClick : Msg -> Attribute Msg
onPreventDefaultClick message =
    onWithOptions "click"
        { defaultOptions | preventDefault = True }
        (preventDefault2
            |> Json.Decode.andThen (maybePreventDefault message)
        )


preventDefault2 : Decoder Bool
preventDefault2 =
    Json.Decode.map2
        (invertedOr)
        (Json.Decode.field "ctrlKey" Json.Decode.bool)
        (Json.Decode.field "metaKey" Json.Decode.bool)


maybePreventDefault : Msg -> Bool -> Decoder Msg
maybePreventDefault msg preventDefault =
    case preventDefault of
        True ->
            Json.Decode.succeed msg

        False ->
            Json.Decode.fail "Normal link"


invertedOr : Bool -> Bool -> Bool
invertedOr x y =
    not (x || y)
