module Dashboard.Msg.User exposing (..)

import Json.Encode as JE
import Dashboard.Action.User as UserAction
import Model.User exposing (..)


type Msg
    = JoinedUserChannel JE.Value
    | LISTUSERSREPLYFROMSERVER JE.Value
    | DELETEUSERRESULT JE.Value
    | DELETEUSERERRORRESULT JE.Value
    | SETACTION (Maybe UserAction.Action)
    | DELETEUSER User
    | DISMISSNOTIFICATION
    | RECEIVEBROADCASTDELETEDUSER JE.Value
    | RECEIVEBROADCASTCREATEDUSER JE.Value
    | CHANGEEMAILFORM String
    | CHANGEPASSWORDFORM String
    | SUBMITFORM
    | CHECKENTERSUBMITFORM Int
    | CREATEUSERERRORRESULT JE.Value
    | CREATEUSERSUCCESSRESULT JE.Value
