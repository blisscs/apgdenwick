module Dashboard.Route exposing (..)

import UrlParser as Url
    exposing
        ( Parser
        , (</>)
        , s
        , int
        , string
        , parseHash
        , oneOf
        , s
        , map
        )


type Route
    = DashboardRoute
    | DashboardUserRoute


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ Url.map DashboardRoute (Url.s "dashboard")
        , Url.map DashboardUserRoute (Url.s "dashboard" </> Url.s "users")
        ]


routeToString : Route -> String
routeToString route =
    case route of
        DashboardRoute ->
            "/dashboard"

        DashboardUserRoute ->
            "/dashboard/users"
