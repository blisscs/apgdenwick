module Dashboard.View.User exposing (..)

import Dashboard.Model exposing (..)
import Dashboard.Msg exposing (..)
import Dashboard.Msg.User as UserMsg
import Model.User exposing (User)
import Html exposing (..)
import Html.Events
    exposing
        ( onClick
        , onInput
        )
import Html.Attributes exposing (..)
import Dashboard.Action.User as UserAction
import Dialog
import On exposing (..)


view : UserModel -> Html Msg
view user_model =
    case user_model.users of
        Nothing ->
            text ""

        Just users ->
            div [ class "users" ]
                [ case user_model.notification of
                    Nothing ->
                        text ""

                    Just ( notificationType, message ) ->
                        div [ class <| "alert alert-" ++ notificationType ]
                            [ text message
                            , button
                                [ type_ "button"
                                , class "close"
                                , onClick (USERGROUPMSG UserMsg.DISMISSNOTIFICATION)
                                ]
                                [ span [] [ text "x" ] ]
                            ]
                , p [ class "text-right" ]
                    [ button
                        [ class "btn btn-info"
                        , type_ "button"
                        , onClick
                            (USERGROUPMSG
                                (UserMsg.SETACTION <|
                                    Just <|
                                        UserAction.SHOWCREATEUSERDIALOG
                                )
                            )
                        ]
                        [ text "New User "
                        , i [ class "fas fa-users" ] []
                        ]
                    ]
                , table [ class "table" ]
                    [ thead []
                        [ tr []
                            [ th [] [ text "ID" ]
                            , th [] [ text "EMAIL" ]
                            , th [] [ text "ACTIONS" ]
                            ]
                        ]
                    , tbody [] (List.map displayUserRow users)
                    ]
                , Dialog.view <| userDialogConfig user_model
                ]


displayUserRow : User -> Html Msg
displayUserRow user =
    tr []
        [ td [] [ text user.id ]
        , td [] [ text user.email ]
        , td []
            [ button
                [ class "btn btn-warning"
                , onClick
                    (USERGROUPMSG
                        (UserMsg.SETACTION <|
                            Just <|
                                UserAction.SHOWDELETEDIALOG user
                        )
                    )
                ]
                [ i [ class "fas fa-trash" ] [] ]
            ]
        ]


userDialogConfig : UserModel -> Maybe (Dialog.Config Msg)
userDialogConfig user_model =
    case user_model.action of
        Just UserAction.SHOWCREATEUSERDIALOG ->
            Just
                { closeMessage =
                    Just <|
                        USERGROUPMSG
                            (UserMsg.SETACTION <|
                                Just <|
                                    UserAction.CLOSEDIALOG
                            )
                , containerClass = Nothing
                , header = Nothing
                , body =
                    Just <|
                        div []
                            [ h3 [] [ text "New User Form" ]
                            , case user_model.user_form_error of
                                Nothing ->
                                    text ""

                                Just user_form_error ->
                                    div [ class "alert alert-danger" ] [ text "Please check inputs again." ]
                            , div [ class "form-group" ]
                                [ label [ for "email" ] [ text "Email" ]
                                , input
                                    [ type_ "text"
                                    , id "email"
                                    , value user_model.user_form.email
                                    , class "form-control"
                                    , onInput (UserMsg.CHANGEEMAILFORM >> USERGROUPMSG)
                                    , onKeyUp (UserMsg.CHECKENTERSUBMITFORM >> USERGROUPMSG)
                                    ]
                                    []
                                , case user_model.user_form_error of
                                    Nothing ->
                                        text ""

                                    Just error ->
                                        ul [ style [ ( "list-style-type", "none" ) ] ] <| List.map (\msg -> li [ class "alert alert-danger" ] [ text msg ]) error.email
                                ]
                            , div [ class "form-group" ]
                                [ label [ for "password" ] [ text "Password" ]
                                , input
                                    [ type_ "password"
                                    , id "password"
                                    , value user_model.user_form.password
                                    , class "form-control"
                                    , onInput (UserMsg.CHANGEPASSWORDFORM >> USERGROUPMSG)
                                    , onKeyUp (UserMsg.CHECKENTERSUBMITFORM >> USERGROUPMSG)
                                    ]
                                    []
                                , case user_model.user_form_error of
                                    Nothing ->
                                        text ""

                                    Just error ->
                                        ul [ style [ ( "list-style-type", "none" ) ] ] <| List.map (\msg -> li [ class "alert alert-danger" ] [ text msg ]) error.password
                                ]
                            , button
                                [ class "btn btn-secondary btn-block btn-lg"
                                , type_ "button"
                                , onClick (USERGROUPMSG UserMsg.SUBMITFORM)
                                ]
                                [ text "Create User" ]
                            ]
                , footer = Nothing
                }

        Just (UserAction.SHOWDELETEDIALOG user) ->
            Just
                { closeMessage =
                    Just <|
                        USERGROUPMSG
                            (UserMsg.SETACTION <|
                                Just <|
                                    UserAction.CLOSEDIALOG
                            )
                , containerClass = Nothing
                , header = Nothing
                , body =
                    Just
                        (div []
                            [ h3 [] [ text "You are about to delete a user" ]
                            , p [] [ text "ID: ", text user.id ]
                            , p [] [ text "Email: ", text user.email ]
                            , p [] [ text "Are U Sure?" ]
                            , p []
                                [ button
                                    [ class "btn btn-info"
                                    , onClick (USERGROUPMSG (UserMsg.SETACTION <| Just <| UserAction.CLOSEDIALOG))
                                    ]
                                    [ text "No. Do not Delete!" ]
                                , text " | "
                                , button
                                    [ class "btn btn-danger"
                                    , onClick (USERGROUPMSG <| UserMsg.DELETEUSER user)
                                    ]
                                    [ text "Yes. Delete This User!" ]
                                ]
                            ]
                        )
                , footer = Nothing
                }

        Just UserAction.CLOSEDIALOG ->
            Nothing

        Nothing ->
            Nothing
