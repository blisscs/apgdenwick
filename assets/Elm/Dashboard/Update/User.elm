module Dashboard.Update.User exposing (..)

import Dashboard.Msg.User as UserMsg
import Dashboard.Msg exposing (..)
import Dashboard.Model exposing (..)
import Dashboard.Action.User as UserAction
import Model.User exposing (User)
import Json.Decode as JD
import Json.Encode as JE
import Model.User
    exposing
        ( usersResponseDecoder
        , userResponseDecoder
        , singleTonErrorDecoder
        , encodeUserForm
        , formErrorResponseDecoder
        )
import Phoenix.Push as Push
import Phoenix


update : UserMsg.Msg -> UserModel -> UserModel
update user_msg user_model =
    case user_msg of
        UserMsg.CREATEUSERSUCCESSRESULT _ ->
            { user_model
                | notification = Just ( "success", "User has been created!" )
                , action = Nothing
            }

        UserMsg.CREATEUSERERRORRESULT result ->
            let
                error_result =
                    JD.decodeValue formErrorResponseDecoder result

                user_form_error =
                    case error_result of
                        Ok user_form_error ->
                            user_form_error.errors

                        Err _ ->
                            Debug.crash "Fail to parse result from failcreate user"
            in
                { user_model | user_form_error = Just user_form_error }

        UserMsg.CHECKENTERSUBMITFORM _ ->
            user_model

        UserMsg.SUBMITFORM ->
            user_model

        UserMsg.CHANGEPASSWORDFORM password ->
            let
                user_form =
                    user_model.user_form

                updated_user_form =
                    { user_form | password = password }
            in
                { user_model | user_form = updated_user_form }

        UserMsg.CHANGEEMAILFORM email ->
            let
                user_form =
                    user_model.user_form

                updated_user_form =
                    { user_form | email = email }
            in
                { user_model | user_form = updated_user_form }

        UserMsg.RECEIVEBROADCASTCREATEDUSER result ->
            let
                user_result =
                    Result.map (\a -> a.user) <|
                        JD.decodeValue
                            userResponseDecoder
                            result

                users =
                    case user_result of
                        Ok user ->
                            case user_model.users of
                                Nothing ->
                                    Just [ user ]

                                Just old_users ->
                                    Just <| List.append old_users [ user ]

                        Err _ ->
                            Debug.crash "Not possible can't parse user Location dashboard/update/user.elm location 1"
            in
                { user_model | users = users }

        UserMsg.RECEIVEBROADCASTDELETEDUSER result ->
            let
                user_result =
                    Result.map (\a -> a.user) <|
                        JD.decodeValue
                            userResponseDecoder
                            result

                users =
                    case user_result of
                        Ok user ->
                            case user_model.users of
                                Nothing ->
                                    Debug.crash "Not Possible"

                                Just old_users ->
                                    Just
                                        (List.filter
                                            (\a -> a /= user)
                                            old_users
                                        )

                        Err _ ->
                            Debug.crash "Not possible can't parse user Location dashboard/update/user.elm location 1"
            in
                { user_model | users = users }

        UserMsg.DISMISSNOTIFICATION ->
            { user_model | notification = Nothing }

        UserMsg.DELETEUSERERRORRESULT result ->
            let
                error_result =
                    JD.decodeValue singleTonErrorDecoder result

                notification =
                    case error_result of
                        Ok error ->
                            Just ( "danger", error.error )

                        Err _ ->
                            Debug.crash "Could not parse delete user error"
            in
                { user_model | notification = notification, action = Nothing }

        UserMsg.DELETEUSERRESULT result ->
            let
                user_result =
                    Result.map (\a -> a.user) <|
                        JD.decodeValue
                            userResponseDecoder
                            result

                notification =
                    case user_result of
                        Ok user ->
                            case user_model.users of
                                Nothing ->
                                    Debug.crash "This case is not possible"

                                Just _ ->
                                    Just ( "success", "Deleted user ID: " ++ user.id ++ " successfully" )

                        Err _ ->
                            Debug.crash "Fail to Parse the result of Delete User"
            in
                { user_model | notification = notification, action = Nothing }

        UserMsg.DELETEUSER user ->
            user_model

        UserMsg.SETACTION action ->
            let
                user_form_error =
                    if action == Just UserAction.SHOWCREATEUSERDIALOG then
                        Nothing
                    else
                        user_model.user_form_error

                user_form =
                    if action == Just UserAction.SHOWCREATEUSERDIALOG then
                        { email = "", password = "" }
                    else
                        user_model.user_form
            in
                { user_model
                    | action = action
                    , user_form = user_form
                    , user_form_error = user_form_error
                }

        UserMsg.JoinedUserChannel _ ->
            user_model

        UserMsg.LISTUSERSREPLYFROMSERVER result ->
            let
                users =
                    Result.map (\a -> a.users) <|
                        JD.decodeValue
                            usersResponseDecoder
                            result

                updated_user_model =
                    case users of
                        Ok users ->
                            { user_model | users = Just users }

                        Err _ ->
                            { user_model | users = Nothing }
            in
                updated_user_model


command : UserMsg.Msg -> String -> UserModel -> Cmd Msg
command user_msg web_socket_url user_model =
    case user_msg of
        UserMsg.CREATEUSERSUCCESSRESULT _ ->
            Cmd.none

        UserMsg.CHECKENTERSUBMITFORM 13 ->
            submit_user_form_command web_socket_url user_model

        UserMsg.CHECKENTERSUBMITFORM _ ->
            Cmd.none

        UserMsg.SUBMITFORM ->
            submit_user_form_command web_socket_url user_model

        UserMsg.CHANGEPASSWORDFORM _ ->
            Cmd.none

        UserMsg.CHANGEEMAILFORM _ ->
            Cmd.none

        UserMsg.DELETEUSERERRORRESULT _ ->
            Cmd.none

        UserMsg.RECEIVEBROADCASTDELETEDUSER _ ->
            Cmd.none

        UserMsg.DISMISSNOTIFICATION ->
            Cmd.none

        UserMsg.DELETEUSERRESULT _ ->
            Cmd.none

        UserMsg.DELETEUSER user ->
            let
                payload =
                    JE.object [ ( "id", JE.string user.id ) ]

                push =
                    Push.init "room:users" "delete_user"
                        |> Push.withPayload payload
                        |> Push.onOk (UserMsg.DELETEUSERRESULT >> USERGROUPMSG)
                        |> Push.onError (UserMsg.DELETEUSERERRORRESULT >> USERGROUPMSG)
            in
                Phoenix.push web_socket_url push

        UserMsg.SETACTION action ->
            Cmd.none

        UserMsg.JoinedUserChannel _ ->
            let
                push =
                    Push.init "room:users" "list_users"
                        |> Push.onOk (UserMsg.LISTUSERSREPLYFROMSERVER >> USERGROUPMSG)
            in
                Phoenix.push web_socket_url push

        UserMsg.LISTUSERSREPLYFROMSERVER result ->
            Cmd.none

        UserMsg.CREATEUSERERRORRESULT result ->
            Cmd.none

        UserMsg.RECEIVEBROADCASTCREATEDUSER _ ->
            Cmd.none


submit_user_form_command : String -> UserModel -> Cmd Msg
submit_user_form_command web_socket_url user_model =
    let
        payload =
            encodeUserForm user_model.user_form

        push =
            Push.init "room:users" "create_user"
                |> Push.withPayload payload
                |> Push.onError (UserMsg.CREATEUSERERRORRESULT >> USERGROUPMSG)
                |> Push.onOk (UserMsg.CREATEUSERSUCCESSRESULT >> USERGROUPMSG)
    in
        Phoenix.push web_socket_url push
