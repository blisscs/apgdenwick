module Dashboard.Sub exposing (..)

import Dashboard.Model exposing (..)
import Dashboard.Msg exposing (..)
import Dashboard.Msg.User as UserMsg
import Phoenix
import Phoenix.Socket as Socket
import Phoenix.Channel as Channel


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        socket_params =
            [ ( "token", model.token ) ]

        socket =
            Socket.init model.paths.web_socket_url
                |> Socket.withParams socket_params

        channel =
            Channel.init "room:users"
                |> Channel.onJoin (UserMsg.JoinedUserChannel >> USERGROUPMSG)
                |> Channel.on "deleted_user" (UserMsg.RECEIVEBROADCASTDELETEDUSER >> USERGROUPMSG)
                |> Channel.on "created_user" (UserMsg.RECEIVEBROADCASTCREATEDUSER >> USERGROUPMSG)
    in
        Sub.batch [ Phoenix.connect socket [ channel ] ]
