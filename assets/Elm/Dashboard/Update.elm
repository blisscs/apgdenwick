module Dashboard.Update exposing (..)

import Dashboard.Msg exposing (..)
import Dashboard.Model exposing (..)
import Dashboard.Route exposing (..)
import Dashboard.Update.User as UserUpdate
import Navigation
import UrlParser as Url


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        updated_model =
            case msg of
                SUB s ->
                    model

                GOTOROUTE r ->
                    model

                URLCHANGE location ->
                    let
                        route =
                            Url.parsePath routeParser location
                    in
                        { model | route = route }

                USERGROUPMSG user_msg ->
                    let
                        user_model =
                            UserUpdate.update user_msg model.user_model
                    in
                        { model | user_model = user_model }

        updated_command =
            command msg model
    in
        ( updated_model, updated_command )


command : Msg -> Model -> Cmd Msg
command msg model =
    case msg of
        GOTOROUTE r ->
            Navigation.newUrl <| routeToString r

        USERGROUPMSG user_msg ->
            UserUpdate.command user_msg model.paths.web_socket_url model.user_model

        URLCHANGE _ ->
            Cmd.none

        SUB _ ->
            Cmd.none
