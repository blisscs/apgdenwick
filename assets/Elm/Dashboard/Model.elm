module Dashboard.Model exposing (..)

import Dashboard.Route exposing (..)
import Dashboard.Action.User as UserAction
import Model.User exposing (..)


type alias Paths =
    { sign_out_path : String
    , web_socket_url : String
    }


type alias Model =
    { paths : Paths
    , token : String
    , route : Maybe Route
    , user_model : UserModel
    }


type alias UserModel =
    { users : Maybe (List User)
    , action : Maybe UserAction.Action
    , notification : Maybe ( NotifcationType, String )
    , user_form : UserForm
    , user_form_error : Maybe UserFormError
    }


type alias NotifcationType =
    String


initialModel : Model
initialModel =
    { paths =
        { sign_out_path = ""
        , web_socket_url = ""
        }
    , token = ""
    , route = Nothing
    , user_model =
        { users = Nothing
        , action = Nothing
        , notification = Nothing
        , user_form = { email = "", password = "" }
        , user_form_error = Nothing
        }
    }
