FROM elixir:1.6.6
RUN apt update
RUN apt install -y inotify-tools curl apt-transport-https build-essential
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -
RUN apt install -y yarn nodejs
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /apgdenwick
ADD . /apgdenwick
WORKDIR /apgdenwick
EXPOSE 4000
RUN mix deps.get
WORKDIR /apgdenwick/assets
RUN yarn install
WORKDIR /apgdenwick
CMD mix phx.server
