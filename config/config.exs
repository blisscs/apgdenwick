# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :apgdenwick,
  namespace: APGDenwick,
  ecto_repos: [APGDenwick.Repo]

# Configures the endpoint
config :apgdenwick, APGDenwickWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rVwvATtQ6gG8WKZoKDMYhiz2CNnJGm6GCCK9fRbZS81SRzbKz7sTTHUD9wUIT5SF",
  render_errors: [view: APGDenwickWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: APGDenwick.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
