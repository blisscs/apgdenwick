use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :apgdenwick, APGDenwickWeb.Endpoint,
  secret_key_base: "Ib4e1D0HgnjHKlW23AHMv1fFG9oNz8itxsw4RH+lXxb4bHH7JFcOHn6Dw71eqmrj"

# Configure your database
config :apgdenwick, APGDenwick.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USER_PROD"),
  password: System.get_env("DB_PASSWORD_PROD"),
  database: System.get_env("DB_NAME_PROD"),
  hostname: System.get_env("DB_HOST_PROD"),
  port: System.get_env("DB_HOST_PROD_PORT"),
  pool_size: 10
