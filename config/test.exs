use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :apgdenwick, APGDenwickWeb.Endpoint,
  http: [port: System.get_env("TEST_PORT")],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure bcrypt_elixir
config :bcrypt_elixir, :log_rounds, 4

# Configure your database
config :apgdenwick, APGDenwick.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USER_TEST"),
  password: System.get_env("DB_PASSWORD_TEST"),
  database: System.get_env("DB_NAME_TEST"),
  hostname: System.get_env("DB_HOST_TEST"),
  port: System.get_env("DB_HOST_TEST_PORT"),
  pool: Ecto.Adapters.SQL.Sandbox
