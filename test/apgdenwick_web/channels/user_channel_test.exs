defmodule APGDenwickWeb.UserChannelTest do
  use APGDenwickWeb.ChannelCase

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User
  alias APGDenwickWeb.UserChannel
  alias APGDenwickWeb.UserSocket

  setup do
    {:ok, %User{} = user} =
      AccountContext.create_user(%{"email" => "admin@example.com", "password" => "password"})

    {:ok, token} =
      AccountContext.sign_in(%{"email" => "admin@example.com", "password" => "password"})

    {:ok, socket} = connect(UserSocket, %{token: token})
    {:ok, _, socket} = subscribe_and_join(socket, UserChannel, "room:users", %{})

    {:ok, socket: socket, user: user}
  end

  test "list_users", %{socket: socket, user: user} do
    ref = push(socket, "list_users", %{})

    users = %{users: [%{id: user.id, email: user.email}]}

    assert_reply(ref, :ok, users)
  end

  test "delete_user where id is same as current_user id", %{socket: socket, user: user} do
    ref = push(socket, "delete_user", %{"id" => user.id})

    assert_reply(ref, :error, %{error: "Can't delete the logged in user."})
  end

  test "delete_user where id is not the same as current_user id", %{socket: socket} do
    {:ok, %User{} = user} =
      AccountContext.create_user(%{"email" => "admin1@example.com", "password" => "password"})

    ref = push(socket, "delete_user", %{"id" => user.id})

    user = %{user: %{id: user.id, email: user.email}}

    assert_broadcast("deleted_user", user)
    assert_reply(ref, :ok, user)
  end

  test "create_user with valid user_params", %{socket: socket} do
    ref =
      push(socket, "create_user", %{
        "user" => %{"email" => "admin1@example.com", "password" => "password"}
      })

    assert_reply(ref, :ok, %{user: %{email: "admin1@example.com"}})
    assert %User{} = AccountContext.get_user_by!(%{email: "admin1@example.com"})
  end

  test "create_user with invalid user_params", %{socket: socket} do
    ref =
      push(socket, "create_user", %{
        "user" => %{"email" => "", "password" => ""}
      })

    assert_reply(ref, :error, %{
      errors: %{email: ["can't be blank"], password: ["can't be blank"]}
    })
  end
end
