defmodule APGDenwickWeb.HelpersTest do
  use APGDenwickWeb.ConnCase

  import APGDenwickWeb.Helpers

  test "apg_socket_url/1" do
    assert apg_socket_url() == "ws://localhost:4001/socket/websocket"
  end
end
