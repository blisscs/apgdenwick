defmodule APGDenwickWeb.DashboardControllerTest do
  use APGDenwickWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/dashboard")
    assert html_response(conn, 200) =~ "A.P.G. Denwick. Dashboard"
  end
end
