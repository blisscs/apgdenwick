defmodule APGDenwickWeb.UserControllerTest do
  use APGDenwickWeb.ConnCase

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User

  setup do
    {:ok, %User{} = user} =
      AccountContext.create_user(%{"email" => "admin@example.com", "password" => "password"})

    {:ok, [user: user]}
  end

  test "GET /api/users", %{conn: conn, user: user} do
    conn = get(conn, "/api/users")

    response = json_response(conn, 200)

    assert response == [%{"id" => user.id, "email" => user.email}]
  end

  test "GET /api/users/[id]", %{conn: conn, user: user} do
    conn = get(conn, "/api/users/#{user.id}")

    response = json_response(conn, 200)

    assert response == %{"id" => user.id, "email" => user.email}
  end
end
