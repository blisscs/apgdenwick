defmodule APGDenwickWeb.PageControllerTest do
  use APGDenwickWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "A.P.G. Denwick. Company Portfolio"
  end
end
