defmodule APGDenwickWeb.SignInControllerTest do
  use APGDenwickWeb.ConnCase

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User

  setup do
    {:ok, %User{} = user} =
      AccountContext.create_user(%{"email" => "admin@example.com", "password" => "password"})

    {:ok, [user: user]}
  end

  test "GET /sign-out", %{conn: conn} do
    conn = get(conn, "/sign-out")
    assert html_response(conn, 200) =~ "A.P.G. Denwick. Sign In"
  end

  test "GET /sign-in", %{conn: conn, user: _user} do
    conn = get(conn, "/sign-in")
    assert html_response(conn, 200) =~ "A.P.G. Denwick. Sign In"
  end

  describe "post /api/sign_in" do
    test "with correct email and password", %{conn: conn, user: _user} do
      conn =
        post(conn, "/api/sign-in", %{
          "sign_in" => %{"email" => "admin@example.com", "password" => "password"}
        })

      assert %{"token" => _token} = json_response(conn, 200)
    end

    test "with incorrect email and password", %{conn: conn, user: _user} do
      conn =
        post(conn, "/api/sign-in", %{
          "sign_in" => %{"email" => "admin@example.com", "password" => "wrongpassword"}
        })

      assert %{"error" => "Email or Password is incorrect"} = json_response(conn, 200)
    end
  end
end
