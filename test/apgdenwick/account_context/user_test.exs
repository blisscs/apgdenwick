defmodule APGDenwick.AccountContext.UserTest do
  use APGDenwick.DataCase

  alias APGDenwick.AccountContext.User

  @valid_attrs %{
    "email" => "admin@example.com",
    "password" => "password"
  }

  @invalid_attrs %{
    "email" => "",
    "password" => ""
  }

  describe "Users" do
    test "valid user" do
      changeset = User.changeset(%User{}, @valid_attrs)

      assert changeset.valid?
    end

    test "invalid user" do
      changeset = User.changeset(%User{}, @invalid_attrs)

      refute changeset.valid?

      assert %{password: ["can't be blank"], email: ["can't be blank"]} = errors_on(changeset)
    end

    test "short password should be invalid" do
      attrs = %{@valid_attrs | "password" => "short"}

      changeset = User.changeset(%User{}, attrs)

      refute changeset.valid?
      assert %{password: ["The password is too short"]} = errors_on(changeset)
    end

    test "invalid email should be invalid" do
      attrs = %{@valid_attrs | "email" => "no at"}

      changeset = User.changeset(%User{}, attrs)

      refute changeset.valid?
      assert %{email: ["has invalid format"]} = errors_on(changeset)
    end
  end
end
