defmodule APGDenwick.AccountContextTest do
  use APGDenwick.DataCase

  alias APGDenwick.AccountContext
  alias APGDenwick.AccountContext.User
  alias Ecto.Changeset
  alias Ecto.UUID

  @valid_user_attrs %{
    "email" => "admin@example.com",
    "password" => "password"
  }

  describe "Account Context" do
    test "create_user/1" do
      assert {:ok, %User{} = user} =
               AccountContext.create_user(%{email: "email@example.com", password: "password"})

      assert user.email == "email@example.com"
      assert user.password == nil

      # Should fail to create with the same email
      assert {:error, %Changeset{} = changeset} =
               AccountContext.create_user(%{email: "email@example.com", password: "password"})

      assert %{email: ["has already been taken"]} = errors_on(changeset)
    end

    setup do
      {:ok, %User{} = user} = AccountContext.create_user(@valid_user_attrs)

      {:ok, [user: user]}
    end

    test "list_users/1", %{user: user} do
      assert [user] == AccountContext.list_users()
    end

    test "update_user/2", %{user: user} do
      {:ok, %User{} = user} =
        AccountContext.update_user(user, %{
          "email" => "update@example.com",
          "password" => "password"
        })

      assert user.email == "update@example.com"
    end

    test "delete_user/1", %{user: user} do
      {:ok, %User{}} = AccountContext.delete_user(user)

      assert nil == AccountContext.get_user(user.id)
    end

    test "get_user!/1", %{user: user} do
      assert fetched_user = AccountContext.get_user!(user.id)
      assert fetched_user.id == user.id

      assert_raise Ecto.NoResultsError, fn ->
        AccountContext.get_user!(UUID.generate())
      end
    end

    test "get_user/1", %{user: user} do
      assert nil == AccountContext.get_user(UUID.generate())

      assert fetched_user = AccountContext.get_user(user.id)
      assert fetched_user.id == user.id
    end

    test "get_user_by/1", %{user: user} do
      assert nil == AccountContext.get_user_by(%{email: "some@email.com"})

      assert fetched_user = AccountContext.get_user_by(%{email: user.email})
      assert fetched_user.id == user.id
    end

    test "get_user_by!/1", %{user: user} do
      assert fetched_user = AccountContext.get_user_by!(%{email: user.email})
      assert fetched_user.id == user.id

      assert_raise Ecto.NoResultsError, fn ->
        AccountContext.get_user_by!(%{id: UUID.generate()})
      end
    end

    test "sign_in/1", %{user: _user} do
      # When User input right user name and password
      assert {:ok, _token} = AccountContext.sign_in(@valid_user_attrs)

      # When User input incorrect password
      attrs = Map.merge(@valid_user_attrs, %{"password" => "somethingelse"})
      assert {:error, "Email or Password is incorrect"} = AccountContext.sign_in(attrs)

      # When Something else
      assert {:error, "Email or Password is incorrect"} =
               AccountContext.sign_in(%{"email" => "someother email", "password" => ""})
    end

    test "verify_user_token/1", %{user: user} do
      {:ok, token} = AccountContext.sign_in(@valid_user_attrs)

      assert user == AccountContext.verify_user_token(token)
    end
  end
end
